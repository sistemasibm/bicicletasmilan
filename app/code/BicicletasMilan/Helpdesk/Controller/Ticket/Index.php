<?php

namespace BicicletasMilan\Helpdesk\Controller\Ticket;

use BicicletasMilan\Helpdesk\Controller\Ticket;
use Magento\Framework\Controller\ResultFactory;

class Index extends Ticket
{
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
