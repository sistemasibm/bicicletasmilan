<?php

namespace BicicletasMilan\Helpdesk\Controller\Adminhtml\Ticket;

use BicicletasMilan\Helpdesk\Controller\Adminhtml\Ticket;

class Grid extends Ticket
{
    public function execute()
    {
        return $this->resultPageFactory->create();
    }
}
