<?php

namespace BicicletasMilan\Helpdesk\Model\ResourceModel\Ticket;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
    * Constructor
    * Configures collection
    *
    * @return void
    */
    protected function _construct()
    {
        $this->_init('BicicletasMilan\Helpdesk\Model\Ticket', 'BicicletasMilan\Helpdesk\Model\ResourceModel\Ticket');
    }
}
