<?php

namespace BicicletasMilan\Helpdesk\Model\Ticket\Grid;

use BicicletasMilan\Helpdesk\Model\Ticket;
use Magento\Framework\Option\ArrayInterface;

class Severity implements ArrayInterface
{
	public function toOptionArray()
	{
		return Ticket::getSeveritiesOptionArray();
	}
}
