<?php

namespace BicicletasMilan\Helpdesk\Model\Ticket\Grid;

use BicicletasMilan\Helpdesk\Model\Ticket;
use Magento\Framework\Option\ArrayInterface;

class Status implements ArrayInterface
{
	public function toOptionArray()
	{
		return Ticket::getStatusesOptionArray();
	}
}
