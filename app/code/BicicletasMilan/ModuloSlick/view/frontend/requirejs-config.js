var config = {
    paths: {
        slick: "BicicletasMilan_ModuloSlick/js/slick.min",
    },
    shim: {
        slick: {
            deps: ["jquery"],
        },
    },
};
