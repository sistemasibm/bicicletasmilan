<?php

declare(strict_types=1);

namespace BicicletasMilan\CustomerInfoDisplay\Block;

use Magento\Framework\View\Element\Template;
use Magento\Customer\Model\SessionFactory;

class CustomerInfoDisplay extends Template
{
    protected $customerSessionFactory;
    public function __construct(
        Template\Context $context,
        SessionFactory $customerSessionFactory,
        array $data = []
    ) {
        
        parent::__construct($context, $data);
        $this->customerSessionFactory = $customerSessionFactory;
        
    }
    public function getCustomerName(){
        $customerSession = $this->customerSessionFactory->create();
        if($customerSession->isLoggedIn()){
            return $customerSession->getCustomer()->getName();
        }
        return null;
    }
}
