<?php
use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'BicicletasMilan_CustomerInfoDisplay',
    __DIR__
);
