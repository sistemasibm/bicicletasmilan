<?php

namespace BicicletasMilan\BackendSlider\Model;

use Magento\Framework\Model\AbstractModel;
use BicicletasMilan\BackendSlider\Api\Data\SlideInterface;

class Slide extends AbstractModel
implements SlideInterface
{
    /**
     * initialize
     * @return void
     */
    protected function _construct()
    {
        $this->_init('BicicletasMilan\BackendSlider\Model\ResourceModel\Slide');
    }

    /**
     * get slice entity
     * @api
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::PROPERTY_ID);
    }

    /**
     * Set Slide entity 'slide_id'
     */
    public function setId($id)
    {
        $this->setData(self::PROPERTY_ID, $id);
        return $this;
    }

    /**
     * Get Slide entity
     * @api
     * @return int|null
     */
    public function getSlideId()
    {
        return $this->getData(self::PROPERTY_SLIDE_ID);
    }

    /**
     * Set Slide entity 'slide_id'
     */
    public function setSlideId($id)
    {
        $this->setData(self::PROPERTY_SLIDE_ID, $id);
        return $this;
    }


    /**
     * Get Slide entity
     * @api
     * @return int|null
     */
    public function getTitle()
    {
        return $this->getData(self::PROPERTY_TITLE);
    }

    /**
     * Set Slide entity 'slide_id'
     */
    public function setTitle($title)
    {
        $this->setData(self::PROPERTY_TITLE, $title);
        return $this;
    }
}
