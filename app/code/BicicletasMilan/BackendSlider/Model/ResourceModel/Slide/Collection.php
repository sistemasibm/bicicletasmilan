<?php

namespace BicicletasMilan\BackendSlider\Model\ResourceModel\Slide;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * BicicletasMilan BackendSlider collection
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'BicicletasMilan\BackendSlider\Model\Slide',
            'BicicletasMilan\BackendSlider\Model\ResourceModel\Slide'
        );
    }
}
