<?php

namespace BicicletasMilan\BackendSlider\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Slide extends AbstractDb
{

    /**
     * Define main table
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'bicicletasmilan_backendslider_slide',
            'slide_id'
        );
    }
}
