<?php

namespace BicicletasMilan\BackendSlider\Api\Data;

interface SlideInterface
{
    const PROPERTY_ID = 'slide_id';
    const PROPERTY_SLIDE_ID = 'slide_id';
    const PROPERTY_TITLE = 'title';

    /**
     * Get the slide entity 'slide_id' property value.
     * @return int|null
     */
    public function getId();

    /**
     * set the slide entity 'slide_id' property value
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * get the slide entity 'slide_id' property value
     * @return int|null
     */
    public function getSlideId();

    /**
     * get the slide entity 'title' property value
     * @return string|null
     */
    public function getTitle();

    /**
     * set the slide entity 'title' property value
     * @param string $title
     * @return $this
     */
    public function setTitle($title);
}
