<?php
namespace BicicletasMilan\BackendSlider\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use BicicletasMilan\BackendSlider\Api\Data\SlideInterface;

interface SlideRepositoryInterface
{
    /**
     * Retrieve slide entity
     * @param int $slideId
     * @return \BicicletasMilan\BackendSlider\Api\Data\SlideInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityExceptionTest
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById( $slideId );

    /**
     * Save slide.
     * @param \BicicletasMilan\BackendSlider\Api\Data\SlideInterface $slide
     * @return \BicicletasMilan\BackendSlider\Api\Data\SlideInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(SlideInterface $slide );

    /**
     * Rerieve slides matching the specified criteria
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchCriteriaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete slide by ID
     * @param int $slideId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NotSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($slideId);
}
