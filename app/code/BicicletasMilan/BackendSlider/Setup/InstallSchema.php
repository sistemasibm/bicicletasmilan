<?php

namespace BicicletasMilan\BackendSlider\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('bicicletasmilan_backendslider_slide'))
            ->addColumn(
                'slide_id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true, 'unsigned' => true,
                    'nullable' => false, 'primary' => true
                ],
                'Slide id'
            )
            ->addColumn(
                'title',
                Table::TYPE_TEXT,
                null,
                [],
                'Title'
            )
            ->setComment('Foggyline Slider Slide');
            
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
