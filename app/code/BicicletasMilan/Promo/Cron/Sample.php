<?php

namespace BicicletasMilan\Promo\Cron;

use Magento\Indexer\Model\IndexerFactory;
use Magento\Indexer\Model\Indexer\CollectionFactory as IndexerCollectionFactory;

class Sample
{
    protected $indexerFactory;
    protected $indexerCollectionFactory;

    public function __construct(
        IndexerFactory $indexerFactory,
        IndexerCollectionFactory $indexerCollectionFactory
    ) {
        $this->indexerFactory = $indexerFactory;
        $this->indexerCollectionFactory = $indexerCollectionFactory;
    }

    public function execute()
    {
        // Obtén todos los indexadores
        $indexerCollection = $this->indexerCollectionFactory->create();
        $allIds = $indexerCollection->getAllIds();

        foreach ($allIds as $id) {
            // Crea un nuevo indexador
            $indexer = $this->indexerFactory->create();
            $indexer->load($id);

            // Reindexa los datos
            $indexer->reindexAll();
        }
    }
}
