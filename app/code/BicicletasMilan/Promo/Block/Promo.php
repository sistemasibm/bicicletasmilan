<?php

declare(strict_types=1);

namespace BicicletasMilan\Promo\Block;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ProductRepository;

class Promo extends Template
{
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $_productRepository;
    private $objectManager;

    public function __construct(
        Template\Context $context,
        ProductRepository $productRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_productRepository = $productRepository;
    }
    /**
     * @param string $productId
     */
    public function getInsigniaParameter($productId)
    {
        $data = null;
        $product = $this->_productRepository->getById($productId);

        if ($product->getInsignia()) {
            $swatcCollection = $this->objectManager->create('Magento\Swatches\Model\ResourceModel\Swatch\Collection');

            $swatcCollection->addFieldtofilter('option_id', $product->getInsignia());
            $item = $swatcCollection->getFirstItem();

            $data['value'] = $product->getAttributeText('insignia');
            $data['color'] = $item->getValue();
        }
        return $data;
    }
}
