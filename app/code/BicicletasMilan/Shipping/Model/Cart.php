<?php

namespace BicicletasMilan\Shipping\Model;

use Closure;
use Magento\Checkout\Model\Cart as CheckoutModel;

class Cart
{
    public function beforeAddProduct(
        CheckoutModel $subjet,
        $productInfo,
        $requestInfo = null
    ) {
        $requestInfo['qty'] = 5;
        return array($productInfo, $requestInfo);
    }

    public function aroundAddProduct(
        CheckoutModel $subjet,
        Closure $proceed,
        $productInfo,
        $requestInfo = null

    ) {
        $requestInfo['qty'] = 4;
        return $proceed($productInfo, $requestInfo);
    }
}
