<?php

namespace BicicletasMilan\BackendDev\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;

class DemoList implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 1, 'label' => __('Option 1')], ['value' => 2, 'label' => __('Option 2')]];
    }
}
