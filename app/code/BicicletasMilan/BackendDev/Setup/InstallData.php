<?php

namespace BicicletasMilan\BackendDev\Setup;

use BicicletasMilan\BackendDev\Model\Demo;
use BicicletasMilan\BackendDev\Model\DemoFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @DemoFactory
     */
    private $demoFactory;

    public function __construct(DemoFactory $demoFactory)
    {
        $this->demoFactory = $demoFactory;
    }

    /**
     * [@inheritdoc]
     * @SuppressWarnings()
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $demoData = [
            'title' => 'Demo Title',
            'is_visible' => 1,
        ];
        $this->createDemo()->setData($demoData)->save();
    }
    public function createDemo()
    {
        return $this->demoFactory->create();
    }
}
