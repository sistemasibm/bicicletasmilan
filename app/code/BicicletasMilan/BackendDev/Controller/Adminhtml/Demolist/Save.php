<?php

namespace BicicletasMilan\BackendDev\Controller\Adminhtml\Demolist;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use BicicletasMilan\BackendDev\Model\DemoFactory;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;

class Save extends Action
{
    const ADMIN_RESOURCE = 'BicicletasMilan_BackendDev::demolist';

    private $demoFactory;
    protected $_coreRegistry;
    protected $reForwardFactory;
    protected $rePageFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DemoFactory $demoFactory,
        ForwardFactory $reForwardFactory,
        PageFactory $rePageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->demoFactory = $demoFactory;
        parent::__construct($context);
        $this->reForwardFactory = $reForwardFactory;
        $this->rePageFactory = $rePageFactory;
    }
    /**
     * Is the user allowed to view the page.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }

    /**
     * Index action
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            if ($id !== null) {
                $demoData = $this->demoFactory->create()->load((int)$id);
            } else {
                $demoData = $this->demoFactory->create();
            }
            $data = $this->getRequest()->getParams();
            $demoData->setData($data)->save();
            $this->messageManager->addSuccess(__('Saved Demolist item.'));
            $resultRedirect->setPath('sample/demolist');
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->_getSession()->setDemoData($data);
            $resultRedirect->setPath('sample/demolist/edit', ['entity_id' => $id]);
        }
        return $resultRedirect;
    }
}
