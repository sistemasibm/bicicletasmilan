<?php

namespace BicicletasMilan\BackendDev\Controller\Adminhtml\Demolist;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use    Magento\Framework\Registry;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Action
{
    protected $_coreRegistry;
    protected $reForwardFactory;
    protected $rePageFactory;
    const ADMIN_RESOURCE = 'BicicletasMilan_BackendDev::demolist';

    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        ForwardFactory $reForwardFactory,
        PageFactory $rePageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->reForwardFactory = $reForwardFactory;
        $this->rePageFactory = $rePageFactory;
    }

    /**
     * Index action
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        return $this->reForwardFactory->create()->forward('new');
    }

    /**
     * Is the user allowed to view the page.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }
}
