<?php

namespace BicicletasMilan\BackendDev\Controller\Adminhtml\Demolist;

use Magento\Backend\App\Action as BackendAction;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action\Context;

class Index extends BackendAction
{
    /**
     * @var PageFactory
     */
    protected $rePageFactory;
    /**
     * @param PageFactory $rePageFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $rePageFactory,
        Context $context
    ) {
        parent::__construct($context);
        $this->rePageFactory = $rePageFactory;
    }
    /**
     * Index action
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $rePage = $this->rePageFactory->create();
        $rePage->getConfig()->getTitle()->prepend(__("Demo List"));
        return $rePage;
    }
    /**
     * Check the permission to execute
     * @return bool
     */
}
