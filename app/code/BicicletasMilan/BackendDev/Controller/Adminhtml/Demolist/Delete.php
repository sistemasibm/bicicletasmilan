<?php

namespace BicicletasMilan\BackendDev\Controller\Adminhtml\Demolist;

use Magento\Backend\App\Action;

class Delete extends Action
{
    const ADMIN_RESOURCE = 'BicicletasMilan_BackendDev::demolist';

    /**
     * Is the user allowed to view the page.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                //Init model and delete
                $model = $this->_objectManager->create('BicicletasMilan\BackendDev\Model\Demo');
                $model->load($id);
                $model->delete();
                //display message
                $this->messageManager->addSuccess(__('The item has been deleted successfully'));
                //go to grid
                return $resultRedirect->setPath('*/*');
            } catch (\Exception $e) {
                //display error message
                $this->messageManager->addError($e->getMessage());
                //go back to edit form
                return $resultRedirect->setPath('*/*edit', ['entity_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We cannot find a listing to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*');
    }
}
