<?php

namespace BicicletasMilan\BackendDev\Controller\Adminhtml\Demolist;

use \Magento\Backend\App\Action;

class NewAction extends Action
{
    const ADMIN_RESOURCE = 'BicicletasMilan_BackendDev::demolist';
    const ADMIN_OP ='New DemoList';
    const ADMIN_ED = 'Edit DemoList';
    
    /**
     * Core registry
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $reForwardFactory;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $rePageFactory;

    /**
     * Initialize Group Controller
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Model\View\Result\ForwardFactory $reForwardFactory,
        \Magento\Framework\View\Result\PageFactory $rePageFactory
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->reForwardFactory = $reForwardFactory;
        $this->rePageFactory = $rePageFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }


    /**
     * Edit DemoList item. Forward to new action.
     * @param \Magento\Backend\Model\View\Result\Page|
     */
    public function execute()
    {
        $demoId = $this->getRequest()->getParam('entity_id');
        $this->_coreRegistry->register('current_entity_id', $demoId);
        $resultPage = $this->rePageFactory->create();
        if ($demoId === null) {
            $resultPage->addBreadcrumb(__(static::ADMIN_OP), __(static::ADMIN_OP));
            $resultPage->getConfig()->getTitle()->prepend(__(static::ADMIN_OP));
        } else {
            $resultPage->addBreadcrumb(__(static::ADMIN_ED), __(static::ADMIN_ED));
            $resultPage->getConfig()->getTitle()->prepend(__(static::ADMIN_ED));
        }

        $resultPage->getLayout()
            ->addBlock('BicicletasMilan\BackendDev\Block\Adminhtml\Demo\Edit', 'demolist', 'content')
            ->setEditMode((bool)$demoId);

        return $resultPage;
    }
}
