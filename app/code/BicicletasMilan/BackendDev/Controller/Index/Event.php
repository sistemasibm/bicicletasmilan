<?php

namespace BicicletasMilan\BackendDev\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Event extends Action
{
    protected $rePageFactory;
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        $this->rePageFactory = $pageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->rePageFactory->create();
        $parameters = [
            'product' => $this->_objectManager->create("Magento\Catalog\Model\Product")->load(1),
            'category' => $this->_objectManager->create("Magento\Catalog\Model\Product")->load(2)
        ];
        $this->_eventManager->dispatch('backenddev_register_visit', $parameters);
        return $resultPage;
    }
}
