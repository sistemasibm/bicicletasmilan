<?php

namespace BicicletasMilan\BackendDev\Block\Adminhtml\Demo;

use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends Container
{
    protected $coreRegistry;
    protected $demoId = false;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_controller = 'adminhtml_demo';
        $this->_blockGroup = 'BicicletasMilan_BackendDev';

        parent::_construct();

        $demoId = $this->getDemoId();
        if (!$demoId) {
            $this->buttonList->remove('delete');
        }
    }

    public function getDemoId()
    {
        if (!$this->demoId) {
            $this->demoId = $this->coreRegistry->registry('current_entity_id');
        }
        return $this->demoId;
    }

    public function getHeaderText()
    {
        $demoId = $this->getDemoId();
        if (!$demoId) {
            return __('New DemoList Item');
        } else {
            return __('Edit DemoList Item');
        }
    }
}
