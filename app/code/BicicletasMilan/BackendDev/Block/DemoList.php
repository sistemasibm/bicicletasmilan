<?php

namespace BicicletasMilan\BackendDev\Block;

use Magento\Framework\View\Element\Template;
use BicicletasMilan\BackendDev\Model\ResourceModel\Demo\Collection as DemoCollection;
use Magento\Store\Model\ScopeInterface;

class DemoList extends Template
{
    /**
     * collection
     * @var DemoCollection
     */
    protected $reDemoCollection;

    protected $reDemoColFactory;

    public function __construct(
        Template\Context $context,
        \BicicletasMilan\BackendDev\Model\ResourceModel\Demo\CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->reDemoColFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param DemoCollection
     */
    public function getDemoItems()
    {
        if (null === $this->reDemoCollection) {
            $this->reDemoCollection = $this->reDemoColFactory->create();
        }
        return $this->reDemoCollection;
    }
}
