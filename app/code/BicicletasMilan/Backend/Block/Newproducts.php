<?php

namespace BicicletasMilan\Backend\Block;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Newproducts extends Template
{
    private $rProductCollectionFactory;

    public function __construct(
        Template\Context $context,
        CollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->rProductCollectionFactory = $productCollectionFactory;
    }

    public function getProducts()
    {
        $collection = $this->rProductCollectionFactory->create();
        $collection->addAttributeToSelect('*')
            ->setOrder('created_at')
            ->setPageSize(5);
        return $collection;
    }
}
