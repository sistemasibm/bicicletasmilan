<?php

namespace BicicletasMilan\Backend\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     * */
    protected $rPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        $this->rPageFactory = $pageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return $this
     */
    public function execute()
    {
        return $this->rPageFactory->create();
    }
}
