<?php

namespace Milan\Tiendas\Controller\Adminhtml\Tiendas;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Milan\Tiendas\Model\Tiendas;

class Save extends Action
{
    protected $uiExamplemodel;
    protected $adminsession;

    public function __construct(
        Action\Context $context,
        Tiendas $uiExamplemodel,
        Session $adminsession
    ) {
        parent::__construct($context);
        $this->uiExamplemodel = $uiExamplemodel;
        $this->adminsession = $adminsession;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $entity_id = $this->getRequest()->getParam('entity_id');
            if ($entity_id) {
                $this->uiExamplemodel->load($entity_id);
            }
            $this->uiExamplemodel->setData($data);

            try {
                $this->uiExamplemodel->save();
                $this->messageManager->addSuccessMessage(__('El registro se ha guardado'));
                $this->adminsession->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' =>
                    $this->uiExamplemodel->getEntityId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Se produjo un error al guardar los datos.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $entity_id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
