# BicicletasMilan Test

Este es un test de Magento para prácticas en el desarrollo de Bicicletas Milán.

## Descripción del Proyecto

Este proyecto es una prueba de Magento para Bicicletas Milán, una marca líder en el sector del ciclismo en Colombia. El objetivo es desarrollar y mejorar la plataforma de comercio electrónico de Bicicletas Milán utilizando Magento.

## Pruebas

Las pruebas se realizan utilizando varias herramientas y técnicas.
